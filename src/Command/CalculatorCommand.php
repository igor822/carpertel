<?php

namespace CasperTel\Command;

use CasperTel\Entity\Plan;
use CasperTel\Service\CallService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CalculatorCommand extends Command
{
    protected function configure()
    {
        $this->setName('calcular')
             ->setDescription('Calculador de tarifas')
             ->addOption('--origem', null, InputOption::VALUE_REQUIRED)
             ->addOption('--destino', null, InputOption::VALUE_REQUIRED)
             ->addOption('--tempo', null, InputOption::VALUE_REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $origin = $input->getOption('origem');
        if ($origin == '') {
            $output->writeln('Favor adicionar a origem da ligação');
            return false;
        }

        if (strpos($origin, '0') === false) {
            $origin = '0' . $origin;
        }

        $destination = $input->getOption('destino');
        if ($destination == '') {
            $output->writeln('Favor adicionar o destino da ligação');
            return false;
        }

        if (strpos($destination, '0') === false) {
            $destination = '0' . $destination;
        }

        $time = $input->getOption('tempo');
        if ($time == '') {
            $output->writeln('Favor adicionar o tempo da ligação');
            return false;
        }

        $this->showValues($origin, $destination, $time, $output);
    }

    private function showValues($origin, $destination, $time, $output)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getHelper('em')->getEntityManager();

        /** @var EntityRepository $planRepository */
        $planRepository = $entityManager->getRepository('CasperTel\Entity\Plan');
        /** @var Plan $plan */
        $plans = $planRepository->findAll();

        /** @var Table $table */
        $table = new Table($output);
        $table->setHeaders(['Plano', 'Valor']);

        $callService = new CallService($entityManager);
        foreach ($plans as $plan) {
            $cost = $callService->calculate(
                $origin,
                $destination,
                (int) $time,
                $plan->getQuantityMinutes()
            );

            $table->addRow([$plan->getName(), $this->buildValuePrice($cost)]);
        }

        $cost = $callService->calculateCost(
            $origin,
            $destination,
            (int) $time,
            null
        );

        $table->addRow(['Normal', $this->buildValuePrice($cost)]);

        $table->render();
    }

    private function buildValuePrice($cost)
    {
        return ($cost !== '-' ? 'R$ ' . number_format($cost, 2, ',', '.') : $cost);
    }
}
