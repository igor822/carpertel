<?php

namespace CasperTel\Service;

use Doctrine\ORM\EntityManager;
use CasperTel\Component\CallCalculatorComponent;
use CasperTel\Entity\CostCall;
use CasperTel\Entity\Plan;
use CasperTel\Repository\CostCallRepository;

class CallService implements ServiceInterface
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $planQuantityMinutes
     * @param string $originCode
     * @param string $destinationCode
     * @param int $timeCall
     * @return float
     */
    final public function calculate($originCode, $destinationCode, $timeCall, $planQuantityMinutes)
    {
        $costCallRepository = $this->getCostCallRepository();

        /** @var CostCall $costCall */
        $costCall = $costCallRepository->findByOriginAndDestination($originCode, $destinationCode);
        if (!$costCall) {
            return '-';
        }

        $callCalculator = new CallCalculatorComponent();
        return $callCalculator->calculate($timeCall, (float) $costCall->getMinutePrice(), $planQuantityMinutes);
    }

    /**
     * @param $originCode
     * @param $destinationCode
     * @param $timeCall
     * @param int|null $planQuantityMinutes
     * @return float
     */
    public function calculateCost($originCode, $destinationCode, $timeCall, $planQuantityMinutes = null)
    {
        if (!is_string($originCode) || !is_string($destinationCode)) {
            throw new \InvalidArgumentException();
        }

        return $this->calculate($originCode, $destinationCode, $timeCall, $planQuantityMinutes);
    }

    /**
     * @return CostCallRepository
     */
    private function getCostCallRepository()
    {
        return $this->entityManager->getRepository('CasperTel\Entity\CostCall');
    }
}
