domain = 'http://CasperTel.dev'

populateTable = (data) ->
  str = '<tr>';
  str += '<td>' + data.originCode + '</td>';
  str += '<td>' + data.destinationCode + '</td>';
  str += '<td>' + data.durationCall + '</td>';
  str += '<td>' + data.planName + '</td>';
  str += '<td class="plan-cost">' + data.costWithPlan + '</td>';
  str += '<td>' + data.costWithoutPlan + '</td>';
  $('#list-cost').show();
  $('tbody').append(str);

populatePlans = (data) ->
  $(data).each (i, item) ->
    $('#plan').append('<option value="' + item.id + '">' + item.name + '</option>');

$(document).ready ->
  $.ajax
    url : domain + '/api/listPlans',
    dataType : 'json',
    success : (data) ->
      populatePlans(data)

  $('#calculateCall').on "submit", ->
    data = {
      originCode : $('#origin').val(),
      destinationCode : $('#destination').val(),
      durationCall : parseInt($('#time').val()),
      plan : $('#plan').val()
    }

    $.ajax
      url : domain + '/api/calculateCall',
      dataType : 'json',
      data : JSON.stringify(data),
      type : 'POST',
      contentType: "application/json; charset=utf-8",
      success : (response) ->
        populateTable(response)

    false