<?php

namespace CasperTel\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="CasperTel\Repository\CostCallRepository")
 * @ORM\Table(name="cost_call")
 */
class CostCall
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="origin_code", type="string", length=5, nullable=false)
     */
    private $originCode;

    /**
     * @ORM\Column(name="destination_code", type="string", length=5, nullable=false)
     */
    private $destinationCode;

    /**
     * @ORM\Column(name="minute_price", type="decimal", precision=10, scale=2, nullable=false)
     *
     */
    private $minutePrice;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getOriginCode()
    {
        return $this->originCode;
    }

    /**
     * @param mixed $originCode
     */
    public function setOriginCode($originCode)
    {
        $this->originCode = $originCode;
    }

    /**
     * @return mixed
     */
    public function getDestinationCode()
    {
        return $this->destinationCode;
    }

    /**
     * @param mixed $destinationCode
     */
    public function setDestinationCode($destinationCode)
    {
        $this->destinationCode = $destinationCode;
    }

    /**
     * @return mixed
     */
    public function getMinutePrice()
    {
        return $this->minutePrice;
    }

    /**
     * @param mixed $minutePrice
     */
    public function setMinutePrice($minutePrice)
    {
        $this->minutePrice = $minutePrice;
    }
}
