<?php

namespace CasperTel\Entity;

use Doctrine\ORM\Mapping\Embeddable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @package CasperTel\Entity
 * @Embeddable
 */
class Call
{


    /**
     * @return mixed
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * @return mixed
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @return int
     */
    public function getTimeCall()
    {
        return $this->timeCall;
    }
}
