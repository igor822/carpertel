<?php

namespace CasperTel\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="plan")
 */
class Plan
{
    const PRICE_ADDITION = 0.10;

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="bigint")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=150, nullable=false, unique=true)
     */
    private $name;

    /**
     * @ORM\Column(name="quantity_minutes", type="integer", length=5, nullable=false)
     */
    private $quantityMinutes;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getQuantityMinutes()
    {
        return $this->quantityMinutes;
    }

    /**
     * @param mixed $quantityMinutes
     */
    public function setQuantityMinutes($quantityMinutes)
    {
        $this->quantityMinutes = $quantityMinutes;
    }
}
