<?php

namespace CasperTel\Tests\Service;

use CasperTel\Entity\CostCall;
use CasperTel\Entity\Plan;
use CasperTel\Service\CallService;

class CallServiceTest extends \PHPUnit_Framework_TestCase
{
    private function mockEntityManager($costCall = null)
    {
        if (is_null($costCall)) {
            $costCall = new CostCall();
        }

        $repoMock = $this->getMockBuilder('CasperTel\Repository\CostCallRepository')
                         ->disableOriginalConstructor()
                         ->getMock();

        $repoMock->expects($this->any())
                 ->method('findByOriginAndDestination')
                 ->will($this->returnValue($costCall));

        $mock = $this->getMockBuilder('\Doctrine\ORM\EntityManager')
                     ->disableOriginalConstructor()
                     ->getMock();

        $mock->expects($this->any())
             ->method('getRepository')
             ->will($this->returnValue($repoMock));

        return $mock;
    }

    public function testCalculateWillReturnCorrectValue()
    {
        $costCall = new CostCall();
        $costCall->setOriginCode('011');
        $costCall->setDestinationCode('012');
        $costCall->setMinutePrice(0.10);

        $entityManager = $this->mockEntityManager($costCall);

        $callService = new CallService($entityManager);
        $cost = $callService->calculateCost('011', '012', 10);

        $this->assertEquals(1, $cost);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCalculateThrowsErrorCasePassedInvalidCode()
    {
        $costCall = new CostCall();
        $costCall->setOriginCode('011');
        $costCall->setDestinationCode('012');
        $costCall->setMinutePrice(0.10);

        $entityManager = $this->mockEntityManager($costCall);

        $callService = new CallService($entityManager);
        $cost = $callService->calculateCost(11, 12, 10);
    }

    public function testCalculateUnknownDeterminedCost()
    {
        $entityManager = $this->mockEntityManager(null);

        $callService = new CallService($entityManager);
        $cost = $callService->calculateCost('012', '011', 100);

        $this->assertEquals('-', $cost);
    }

    /**
     * @param $quantityMinutes
     * @param $minutePrice
     * @param $timeCall
     * @param $mustBe
     * @dataProvider callWithPlanProvider
     */
    public function testCallWithPlanAllowed($quantityMinutes, $minutePrice, $timeCall, $mustBe)
    {
        $plan = new Plan();
        $plan->setName('Test');
        $plan->setQuantityMinutes($quantityMinutes);

        $costCall = new CostCall();
        $costCall->setOriginCode('011');
        $costCall->setDestinationCode('012');
        $costCall->setMinutePrice($minutePrice);

        $entityManager = $this->mockEntityManager($costCall);

        $callService = new CallService($entityManager);
        $cost = $callService->calculateCost('011', '016', $timeCall, $plan->getQuantityMinutes());

        $this->assertEquals($mustBe, $cost);
    }

    /**
     * @param $minutePrice
     * @param $timeCall
     * @param $mustBe
     * @dataProvider simpleCalculateProvider
     */
    public function testGenericCalculateWithoutPlanReturnCorrectCostValue($timeCall, $minutePrice, $mustBe)
    {
        $plan = new Plan();
        $plan->setName('Test');

        $costCall = new CostCall();
        $costCall->setOriginCode('011');
        $costCall->setDestinationCode('012');
        $costCall->setMinutePrice($minutePrice);

        $entityManager = $this->mockEntityManager($costCall);

        $callService = new CallService($entityManager);
        $cost = $callService->calculateCost('011', '016', $timeCall);

        $this->assertEquals($mustBe, $cost);
    }

    /**
     * @param $quantityMinutes
     * @param $minutePrice
     * @param $timeCall
     * @param $mustBe
     * @dataProvider callWithPlanProvider
     */
    public function testGenericCalculateWithPlanReturnCorrectCostValue(
        $quantityMinutes,
        $minutePrice,
        $timeCall,
        $mustBe
    ) {
        $plan = new Plan();
        $plan->setName('Test');
        $plan->setQuantityMinutes($quantityMinutes);

        $costCall = new CostCall();
        $costCall->setOriginCode('011');
        $costCall->setDestinationCode('012');
        $costCall->setMinutePrice($minutePrice);

        $entityManager = $this->mockEntityManager($costCall);

        $callService = new CallService($entityManager);
        $cost = $callService->calculateCost('011', '012', $timeCall, $plan->getQuantityMinutes());

        $this->assertEquals($mustBe, $cost);
    }

    public function callWithPlanProvider()
    {
        return [
            [30, 1.90, 40, (10 * (1.90 + (Plan::PRICE_ADDITION * 1.90)))],
            [60, 1.90, 50, 0],
            [120, 2.90, 170, (50 * (2.90 + (Plan::PRICE_ADDITION * 2.90)))]
        ];
    }

    public function simpleCalculateProvider()
    {
        return [
            [10, 1.0, 10],
            [10, 1.9, 19.00],
        ];
    }
}
