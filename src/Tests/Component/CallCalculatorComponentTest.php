<?php

namespace CasperTel\Tests\Component;

use CasperTel\Component\CallCalculatorComponent;
use CasperTel\Entity\Plan;

class CallCalculatorComponentTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider simpleCalculateProvider
     */
    public function testSimpleCalculateWillReturnCorrectValue($timeCall, $minutePrice, $mustBe)
    {
        $callCalculator = new CallCalculatorComponent();
        $cost = $callCalculator->calculate($timeCall, $minutePrice);

        $this->assertEquals($mustBe, $cost);
    }

    /**
     * @dataProvider calculationWithPlanProvider
     */
    public function testCalculationWithPlanWillReturnCorrectValue($allowedMinutes, $minutePrice, $timeCall, $mustBe)
    {
        $callCalculator = new CallCalculatorComponent();
        $cost = $callCalculator->calculate($timeCall, $minutePrice, $allowedMinutes);

        $this->assertEquals($mustBe, $cost);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testWithInvalidArgumentsWillThrowAnError()
    {
        $callCalculator = new CallCalculatorComponent();
        $callCalculator->calculate('10', '10');
    }

    public function simpleCalculateProvider()
    {
        return [
            [10, 1.0, 10],
            [10, 1.9, 19.00],
            [20, 0.0, '-'],
        ];
    }

    public function calculationWithPlanProvider()
    {
        return [
            [30, 1.90, 40, (10 * (1.90 + (Plan::PRICE_ADDITION * 1.90)))],
            [60, 1.90, 50, 0],
            [120, 2.90, 170, (50 * (2.90 + (Plan::PRICE_ADDITION * 2.90)))]
        ];
    }
}
