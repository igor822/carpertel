<?php

namespace CasperTel\Controller;

use Doctrine\ORM\EntityRepository;
use CasperTel\Entity\Plan;
use CasperTel\Service\CallService;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class HomeController
{
    public function plansAction(Application $application)
    {
        /** @var EntityRepository $planRepository */
        $planRepository = $application['orm.em']->getRepository('CasperTel\Entity\Plan');

        $plans = $planRepository->findAll();
        $data = [];
        /** @var Plan $plan */
        foreach ($plans as $plan) {
            $data[] = [
                'id' => $plan->getId(),
                'name' => $plan->getName(),
                'quantityMinutes' => $plan->getQuantityMinutes()
            ];
        }

        return new JsonResponse($data);
    }

    public function calculateCallAction(Application $application, Request $request)
    {
        $data = $request->request->all();

        /** @var EntityRepository $planRepository */
        $planRepository = $application['orm.em']->getRepository('CasperTel\Entity\Plan');
        /** @var Plan $plan */
        $plan = $planRepository->findOneBy(['id' => $data['plan'] ? $data['plan'] : 1]);

        try {
            $callService = new CallService($application['orm.em']);
            $costWithPlan = $callService->calculateCost(
                $data['originCode'],
                $data['destinationCode'],
                (int) $data['durationCall'],
                $plan->getQuantityMinutes()
            );

            $cost = $callService->calculateCost(
                $data['originCode'],
                $data['destinationCode'],
                (int) $data['durationCall'],
                null
            );
        } catch (\InvalidArgumentException $e) {
            return new JsonResponse(['error' => true, 'message' => 'An error occurred']);
        }

        $data = [
            'originCode' => $data['originCode'],
            'destinationCode' => $data['destinationCode'],
            'durationCall' => $data['durationCall'],
            'costWithPlan' => ($costWithPlan !== '-' ? 'R$ ' . number_format($costWithPlan, 2, ',', '.') : $costWithPlan),
            'costWithoutPlan' => ($cost !== '-' ? 'R$ ' . number_format($cost, 2, ',', '.') : $cost),
            'planName' => $plan->getName()
        ];

        return new JsonResponse($data);
    }
}
