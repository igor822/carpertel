<?php

namespace CasperTel\Component;

use CasperTel\Component\CallCalculator\GenericCalculator;
use CasperTel\Component\CallCalculator\PlanCalculator;
use CasperTel\Entity\Plan;

class CallCalculatorComponent
{
    /**
     * @param $timeCall
     * @param $minutePrice
     * @param int|null $planQuantityMinutes
     * @return float|int|string
     */
    public function calculate($timeCall, $minutePrice, $planQuantityMinutes = null)
    {
        if (!is_int($timeCall) || !is_float($minutePrice)) {
            throw new \InvalidArgumentException();
        }

        if (null === $planQuantityMinutes) {
            $calculator = new GenericCalculator($timeCall, $minutePrice);
        } else {
            $calculator = new PlanCalculator($planQuantityMinutes, $timeCall, $minutePrice);
        }

        return $calculator->calculate();
    }
}
