<?php

namespace CasperTel\Component\CallCalculator;

interface CalculatorInterface
{
    public function calculate();
}
