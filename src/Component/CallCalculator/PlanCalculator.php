<?php

namespace CasperTel\Component\CallCalculator;

use CasperTel\Entity\Plan;

class PlanCalculator implements CalculatorInterface
{
    private $planQuantityMinutes;

    private $timeCall;

    private $minutePrice = null;

    public function __construct($planQuantityMinutes, $timeCall, $minutePrice = null)
    {
        $this->planQuantityMinutes = $planQuantityMinutes;
        $this->timeCall = $timeCall;
        $this->minutePrice = $minutePrice;
    }

    public function calculate()
    {
        if (!is_int($this->timeCall)) {
            throw new \InvalidArgumentException();
        }

        if (!$this->minutePrice) {
            return '-';
        }

        if ($this->timeCall <= $this->planQuantityMinutes) {
            return 0.0;
        }

        $timeCall = $this->timeCall - $this->planQuantityMinutes;
        return $timeCall * ($this->minutePrice + (Plan::PRICE_ADDITION * $this->minutePrice));
    }
}
