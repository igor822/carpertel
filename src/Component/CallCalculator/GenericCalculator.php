<?php

namespace CasperTel\Component\CallCalculator;

class GenericCalculator implements CalculatorInterface
{
    private $timeCall;

    private $minutePrice = null;

    public function __construct($timeCall, $minutePrice = null)
    {
        $this->timeCall = $timeCall;
        $this->minutePrice = $minutePrice;
    }

    public function calculate()
    {
        if (!is_int($this->timeCall)) {
            throw new \InvalidArgumentException();
        }

        if (!$this->minutePrice) {
            return '-';
        }

        return $this->timeCall * $this->minutePrice;
    }
}
