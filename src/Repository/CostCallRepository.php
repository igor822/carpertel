<?php

namespace CasperTel\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class CostCallRepository
 * @package CasperTel\Repository
 */
class CostCallRepository extends EntityRepository
{
    /**
     * @param $originCode
     * @param $destinationCode
     * @return array
     */
    public function findByOriginAndDestination($originCode, $destinationCode)
    {
        return parent::findOneBy(['originCode' => $originCode, 'destinationCode' => $destinationCode]);
    }

    /**
     * @param $originCode
     * @param $destinationCode
     * @return array
     */
    public function findAllByOriginAndDestination($originCode, $destinationCode)
    {
        return parent::findBy(['originCode' => $originCode, 'destinationCode' => $destinationCode]);
    }
}
