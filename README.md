CasperTel
========

O projeto consiste no desenvolvimento do teste [Cálculo de Tarifas Telefônicas](https://bitbucket.org/casperlibero/fcl-dev-test/src/3079d4b903fcd0599f988ef7d052762bce9f3bb8/TESTE-2.md?fileviewer=file-view-default)

# Dependência

* PHP 5.5+
* MySQL

# Configurando projeto

### Composer

Ao rodar `composer install` será instalado todas as dependências necessárias para o projeto rodar tranquilamente. 
Será solicitado na instalação das dependências os dados do banco de dados.

### Banco dedados

O arquivo ./database.sql possui a estrutura e dados para o funcionamento do projeto, como custo de ligações de DDD para DDD.

# Execução

Para executar o comando basta dar permissão no arquivo `./calculator` com `chmod +x ./calculator`. O comando para calcular a ligação:

```sh
./calculator calcular --origem=11 --destino=17 --tempo=200
```