<?php

define('APP_PATH', realpath(__DIR__));

require_once __DIR__.'/../vendor/autoload.php';

use Silex\Application;

$app = new Application();

$app->before(function (\Symfony\Component\HttpFoundation\Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : []);
    }
});


//handling CORS preflight request
$app->before(function (\Symfony\Component\HttpFoundation\Request $request) use ($app) {
    if ($request->getMethod() === 'OPTIONS') {
        $response = new \Symfony\Component\HttpFoundation\Response();
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'GET,POST,PUT,PATCH,DELETE,OPTIONS');
        $response->headers->set('Access-Control-Allow-Headers', 'Content-Type, X-TOKEN');
        return $response->send();
    }
}, Application::EARLY_EVENT);

require __DIR__ . '/config/routes.php';
require __DIR__ . '/config/services.php';

return $app;
