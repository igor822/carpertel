<?php

use DerAlex\Silex\YamlConfigServiceProvider;
use Dflydev\Silex\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use Knp\Provider\ConsoleServiceProvider;
use Saxulum\DoctrineOrmManagerRegistry\Silex\Provider\DoctrineOrmManagerRegistryProvider;
use Silex\Provider\DoctrineServiceProvider;

/** @var \Silex\Application $app */
$app->register(new YamlConfigServiceProvider(__DIR__ . '/parameters.yml'));

$app->register(new DoctrineServiceProvider(), array(
    'db.options' => $app['config']['database']
));

$app->register(new DoctrineOrmServiceProvider, array(
    'orm.em.options' => array(
        'mappings' => array(
            array(
                'use_simple_annotation_reader' => false,
                'namespace' => 'CasperTel\Entity',
                'type' => 'annotation',
                'path' => realpath(__DIR__ . '/../../src/Entity'),
            )
        ),
    ),
));
$app->register(new DoctrineOrmManagerRegistryProvider());

$loader = require __DIR__ . '/../../vendor/autoload.php';
\Doctrine\Common\Annotations\AnnotationRegistry::registerLoader(array($loader, 'loadClass'));

/**
 * Console Provider
 */
$app->register(new ConsoleServiceProvider(), array(
    'console.name' => 'CasperTel',
    'console.version' => '1.0.0',
    'console.project_directory' => __DIR__ . '/..'
));

$app->register(new Silex\Provider\HttpCacheServiceProvider(), array(
    'http_cache.cache_dir' => __DIR__.'/cache/',
));