<?php

return [
    'origins' => [
        '011' => 'São Paulo',
        '016' => 'Ribeirão Preto',
        '017' => 'Mirassol',
        '018' => 'Tupi Paulista'
    ],
    'plans' => [
        'FaleMais 30' => 30,
        'FaleMais 60' => 60,
        'FaleMais 120' => 120
    ],
    'costs' => [
        [
            'from' => '011',
            'to' => '016',
            'cost' => 1.90
        ],
        [
            'from' => '016',
            'to' => '011',
            'cost' => 2.90
        ],
        [
            'from' => '011',
            'to' => '017',
            'cost' => 1.70
        ],
        [
            'from' => '017',
            'to' => '011',
            'cost' => 2.70
        ],
        [
            'from' => '011',
            'to' => '018',
            'cost' => 0.90
        ],
        [
            'from' => '018',
            'to' => '011',
            'cost' => 1.90
        ]
    ]
];
