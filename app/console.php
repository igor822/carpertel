#!/usr/bin/env php
<?php
set_time_limit(0);
$app = require_once __DIR__.'/bootstrap.php';
/** @var \Knp\Console\Application $application */
$application = $app['console'];
$application->setHelperSet(new Symfony\Component\Console\Helper\HelperSet(array(
    'db' => new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($app["db"]),
    'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($app["orm.em"]),
    'dialog' => new \Symfony\Component\Console\Helper\DialogHelper(false)
)));
$application->add(new \Doctrine\ORM\Tools\Console\Command\ClearCache\MetadataCommand);
$application->add(new \Doctrine\ORM\Tools\Console\Command\ClearCache\QueryCommand);
$application->add(new \Doctrine\ORM\Tools\Console\Command\ClearCache\ResultCommand);
$application->add(new \Doctrine\ORM\Tools\Console\Command\SchemaTool\CreateCommand);
$application->add(new \Doctrine\ORM\Tools\Console\Command\SchemaTool\DropCommand);
$application->add(new \Doctrine\ORM\Tools\Console\Command\SchemaTool\UpdateCommand);
$application->add(new \Doctrine\ORM\Tools\Console\Command\ConvertDoctrine1SchemaCommand);
$application->add(new \Doctrine\ORM\Tools\Console\Command\ConvertMappingCommand);
$application->add(new \Doctrine\ORM\Tools\Console\Command\EnsureProductionSettingsCommand);
$application->add(new \Doctrine\ORM\Tools\Console\Command\GenerateEntitiesCommand);
$application->add(new \Doctrine\ORM\Tools\Console\Command\GenerateProxiesCommand);
$application->add(new \Doctrine\ORM\Tools\Console\Command\GenerateRepositoriesCommand);
$application->add(new \Doctrine\ORM\Tools\Console\Command\InfoCommand);
$application->add(new \Doctrine\ORM\Tools\Console\Command\RunDqlCommand);
$application->add(new \Doctrine\ORM\Tools\Console\Command\ValidateSchemaCommand);
$application->add(new \Doctrine\DBAL\Tools\Console\Command\ImportCommand);
$application->add(new \Doctrine\DBAL\Tools\Console\Command\ReservedWordsCommand);
$application->add(new \Doctrine\DBAL\Tools\Console\Command\RunSqlCommand);
$application->run();
